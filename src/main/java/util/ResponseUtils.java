package util;

import java.io.DataOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.HttpResponse;
import model.StatusCode;

/**
 * @author sanggeun.choi
 */
public class ResponseUtils {
	private static final Logger log = LoggerFactory.getLogger(ResponseUtils.class);

	public static void responseHeader(DataOutputStream dos, HttpResponse response) {
		try {
			dos.writeBytes(response.getStatusCode().getHeaderLine() + " \r\n");

			if (response.getContentType() != null) {
				dos.writeBytes("Content-Type: " + response.getContentType().getTypeValue() + "\r\n");
			}

			if (response.getContents() != null) {
				dos.writeBytes("Content-Length: " + response.getContents().getBytes().length + "\r\n");
			}

			if (response.getStatusCode() == StatusCode.REDIRECT && response.getLocation() != null) {
				dos.writeBytes("Location: " + response.getLocation() + "\r\n");
			}

			for (String key : response.getCookies().keySet()) {
				dos.writeBytes(String.format("Set-Cookie: %s=%s\r\n", key, response.getCookies().get(key)));
			}

			dos.writeBytes("\r\n");
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}

	public static void responseBody(DataOutputStream dos, HttpResponse response) {
		try {
			dos.write(response.getContents().getBytes(), 0, response.getContents().getBytes().length);
			dos.flush();
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}
}
