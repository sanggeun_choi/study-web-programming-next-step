package util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sanggeun.choi
 */
public class WebAppUtils {
	private static final Logger log = LoggerFactory.getLogger(WebAppUtils.class);

	public static String readWebAppFile(String url) {
		try {
			return new String(Files.readAllBytes(new File("./webapp" + url).toPath()));
		} catch (IOException e) {
			log.error(e.getMessage());
		}

		return null;
	}
}
