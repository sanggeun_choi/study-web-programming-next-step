package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.HttpRequest;

/**
 * @author sanggeun.choi
 */
public class RequestUtils {
	private static final Logger log = LoggerFactory.getLogger(RequestUtils.class);
	private static final String ENCODING = "UTF-8";

	public static HttpRequest getHttpRequest(InputStream in) {
		HttpRequest httpRequest = new HttpRequest();
		String line;

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			while ((line = br.readLine()) != null && !line.isEmpty()) {
				log.debug(line);
				String[] info = line.split(" ");

				if (line.startsWith("GET")) {
					processHttpRequestAboutGET(httpRequest, info);
				} else if (line.startsWith("POST")) {
					processHttpRequestAboutPOST(httpRequest, info);
				} else if (line.startsWith("Content-Length:")) {
					processHttpRequestAboutContentLength(httpRequest, info);
				} else if (line.startsWith("Cookie:")) {
					processHttpRequestAboutCookie(httpRequest, info);
				} else if (line.startsWith("Accept:")) {
					processHttpRequestAboutAccept(httpRequest, info);
				}
			}

			readQueryStringFromHeaderBody(httpRequest, br);
			processHttpRequestAboutQueryString(httpRequest);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}

		return httpRequest;
	}

	private static void processHttpRequestAboutGET(HttpRequest httpRequest, String[] info) {
		String[] urlInfo = info[1].split("[?]");

		httpRequest.setMethod(info[0]);
		httpRequest.setUrl(urlInfo[0]);

		if (urlInfo.length > 1) {
			httpRequest.setQueryString(urlInfo[1]);
		}
	}

	private static void processHttpRequestAboutPOST(HttpRequest httpRequest, String[] info) {
		httpRequest.setMethod(info[0]);
		httpRequest.setUrl(info[1]);
	}

	private static void processHttpRequestAboutContentLength(HttpRequest httpRequest, String[] info) {
		httpRequest.setContentLength(Long.parseLong(info[1]));
	}

	private static void processHttpRequestAboutQueryString(HttpRequest httpRequest) throws UnsupportedEncodingException {
		if (httpRequest.getQueryString() == null) {
			return;
		}

		String decodedQueryString = URLDecoder.decode(httpRequest.getQueryString(), ENCODING);

		httpRequest.setParams(HttpRequestUtils.parseQueryString(decodedQueryString));
	}

	private static void processHttpRequestAboutCookie(HttpRequest httpRequest, String[] info) {
		httpRequest.setCookies(HttpRequestUtils.parseCookies(info[1]));
	}

	private static void processHttpRequestAboutAccept(HttpRequest httpRequest, String[] info) {
		String[] acceptInfo = info[1].split(",");
		httpRequest.setAccept(acceptInfo[0]);
	}

	private static void readQueryStringFromHeaderBody(HttpRequest httpRequest, BufferedReader br) throws IOException {
		if (httpRequest.getContentLength() > 0) {
			httpRequest.setQueryString(IOUtils.readData(br, (int) httpRequest.getContentLength()));
		}
	}
}
