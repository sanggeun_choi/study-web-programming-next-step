package servlet;

import model.ContentType;
import model.HttpRequest;
import model.HttpResponse;
import util.WebAppUtils;

/**
 * @author sanggeun.choi
 */
public class WebAppServlet extends Servlet {
	public WebAppServlet(String url) {
		super(url);
	}

	@Override
	public HttpResponse doService(HttpRequest httpRequest) {
		response.setContentType(ContentType.getContentTypeByTypeValue(httpRequest.getAccept()));
		response.setContents(WebAppUtils.readWebAppFile(httpRequest.getUrl()));
		return response;
	}
}
