package servlet;

import model.ContentType;
import model.HttpRequest;
import model.HttpResponse;
import model.StatusCode;

/**
 * Created by john on 2017-01-30.
 */
public abstract class Servlet {
    protected String url;
    protected HttpResponse response;

    public Servlet(String url) {
        this.url = url;
        this.response = makeDefaultResponse();
    }

    private HttpResponse makeDefaultResponse() {
        HttpResponse response = new HttpResponse();
        response.setStatusCode(StatusCode.OK);
        response.setContentType(ContentType.HTML);
        return response;
    }

    public abstract HttpResponse doService(HttpRequest httpRequest);
}
