package servlet;

import model.HttpRequest;
import model.HttpResponse;
import util.WebAppUtils;

/**
 * Created by john on 2017-01-30.
 */
public class IndexServlet extends Servlet {
    public IndexServlet(String url) {
        super(url);
    }

    @Override
    public HttpResponse doService(HttpRequest httpRequest) {
        response.setContents(WebAppUtils.readWebAppFile(httpRequest.getUrl()));
        return response;
    }
}
