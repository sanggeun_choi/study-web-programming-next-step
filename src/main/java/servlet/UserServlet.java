package servlet;

import java.util.Map;

import db.DataBase;
import model.HttpRequest;
import model.HttpResponse;
import model.StatusCode;
import model.User;
import util.WebAppUtils;

/**
 * @author sanggeun.choi
 */
public class UserServlet extends Servlet {
	public UserServlet(String url) {
		super(url);
	}

	@Override
	public HttpResponse doService(HttpRequest request) {
		switch (url) {
			case "/user/form.html" :
			case "/user/login.html" :
			case "/user/login_failed.html" :
				return doServiceHTML(request);
			case "/user/create" :
				return doServiceUserCreate(request);
			case "/user/login" :
				return doServiceUserLogin(request);
			case "/user/list" :
				return doServiceUserList(request);
		}

		return response;
	}

	private HttpResponse doServiceHTML(HttpRequest request) {
		response.setContents(WebAppUtils.readWebAppFile(request.getUrl()));
		return response;
	}

	private HttpResponse doServiceUserCreate(HttpRequest request) {
		Map<String, String> params = request.getParams();
		User user = new User(params.get("userId"), params.get("password"), params.get("name"), params.get("email"));

		DataBase.addUser(user);

		response.setStatusCode(StatusCode.REDIRECT);
		response.setLocation("/index.html");
		return response;
	}

	private HttpResponse doServiceUserLogin(HttpRequest request) {
		String userId = request.getParams().get("userId");
		String password = request.getParams().get("password");
		User user = DataBase.findUserById(userId);

		if (user != null && userId.equals(user.getUserId()) && password.equals(user.getPassword())) {
			response.addCookie("logined", "true");
			response.setLocation("/index.html");
		} else {
			response.addCookie("logined", "false");
			response.setLocation("/user/login_failed.html");
		}

		response.setStatusCode(StatusCode.REDIRECT);
		return response;
	}

	private HttpResponse doServiceUserList(HttpRequest request) {
		if (Boolean.parseBoolean(request.getCookieValue("logined"))) {
			response.setContents(getUserListHTML());
		} else {
			response.setStatusCode(StatusCode.REDIRECT);
			response.setLocation("/user/login.html");
		}

		return response;
	}

	private String getUserListHTML() {
		StringBuilder stringBuilder = new StringBuilder();

		for (User user : DataBase.findAll()) {
			stringBuilder.append(user.getUserId() + " " + user.getName() + "<br>");
		}

		return stringBuilder.toString();
	}
}
