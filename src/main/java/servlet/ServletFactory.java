package servlet;

/**
 * Created by john on 2017-01-30.
 */
public class ServletFactory {
    public static Servlet getServlet(String url) {
        if (url.startsWith("/index")) {
            return new IndexServlet(url);
        } else if (url.startsWith("/user")) {
            return new UserServlet(url);
        }

        return new WebAppServlet(url);
    }
}
