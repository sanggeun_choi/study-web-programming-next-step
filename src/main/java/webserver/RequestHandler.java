package webserver;


import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.HttpRequest;
import model.HttpResponse;
import servlet.ServletFactory;
import util.RequestUtils;
import util.ResponseUtils;

public class RequestHandler extends Thread {
    private static final Logger log = LoggerFactory.getLogger(RequestHandler.class);

    private Socket connection;

    public RequestHandler(Socket connectionSocket) {
        this.connection = connectionSocket;
    }

    public void run() {
        log.debug("New Client Connect! Connected IP : {}, Port : {}", connection.getInetAddress(), connection.getPort());

        try (InputStream in = connection.getInputStream(); OutputStream out = connection.getOutputStream()) {
            HttpRequest request = RequestUtils.getHttpRequest(in);
            HttpResponse response = ServletFactory.getServlet(request.getUrl()).doService(request);

            DataOutputStream dos = new DataOutputStream(out);
            ResponseUtils.responseHeader(dos, response);
            ResponseUtils.responseBody(dos, response);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
