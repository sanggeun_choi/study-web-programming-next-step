package model;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

/**
 * @author sanggeun.choi
 */
@Data
public class HttpResponse {
	private StatusCode statusCode;
	private ContentType contentType;
	private String location;
	private String contents;
	private Map<String, String> cookies = new HashMap<String, String>();

	public void addCookie(String key, String value) {
		cookies.put(key, value);
	}
}
