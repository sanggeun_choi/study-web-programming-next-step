package model;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

/**
 * @author sanggeun.choi
 */
@Data
public class HttpRequest {
	private String url;
	private String method;
	private String queryString;
	private String accept;
	private Map<String, String> params;
	private Map<String, String> cookies = new HashMap<String, String>();
	private long contentLength;

	public String getCookieValue(String cookieName) {
		return cookies.get(cookieName);
	}
}
