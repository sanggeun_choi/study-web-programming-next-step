package model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sanggeun.choi
 */
public enum ContentType {
	HTML("text/html;charset=utf-8"),
	CSS("text/css"),
	;

	private String typeValue;
	private static Map<String, ContentType> typeValueMap = new HashMap<String, ContentType>();

	static {
		for (ContentType contentType : values()) {
			typeValueMap.put(contentType.typeValue, contentType);
		}
	}

	ContentType(String typeValue) {
		this.typeValue = typeValue;
	}

	public String getTypeValue() {
		return typeValue;
	}

	public static ContentType getContentTypeByTypeValue(String typeValue) {
		return typeValueMap.get(typeValue);
	}
}
