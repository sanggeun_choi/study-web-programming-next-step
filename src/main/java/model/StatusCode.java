package model;

/**
 * @author sanggeun.choi
 */
public enum StatusCode {
	OK("200", "HTTP/1.1 200 OK"),
	REDIRECT("302", "HTTP/1.1 302 Redirect");

	private String code;
	private String headerLine;

	StatusCode(String code, String headerLine) {
		this.code = code;
		this.headerLine = headerLine;
	}

	public String getHeaderLine() {
		return headerLine;
	}
}
